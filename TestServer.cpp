/*----- PROTECTED REGION ID(TestServer.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        TestServer.cpp
//
// description : C++ source for the TestServer class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               TestServer are implemented in this file.
//
// project :     TestServer
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <TestServer.h>
#include <TestServerClass.h>

/*----- PROTECTED REGION END -----*/	//	TestServer.cpp

/**
 *  TestServer class description:
 *    
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//  TestValue  |  Tango::DevDouble	Scalar
//================================================================

namespace TestServer_ns
{
/*----- PROTECTED REGION ID(TestServer::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	TestServer::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : TestServer::TestServer()
 *	Description : Constructors for a Tango device
 *                implementing the classTestServer
 */
//--------------------------------------------------------
TestServer::TestServer(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(TestServer::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::constructor_1
}
//--------------------------------------------------------
TestServer::TestServer(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(TestServer::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::constructor_2
}
//--------------------------------------------------------
TestServer::TestServer(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(TestServer::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : TestServer::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void TestServer::delete_device()
{
	DEBUG_STREAM << "TestServer::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(TestServer::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::delete_device
	delete[] attr_TestValue_read;
}

//--------------------------------------------------------
/**
 *	Method      : TestServer::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void TestServer::init_device()
{
	DEBUG_STREAM << "TestServer::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(TestServer::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::init_device_before
	
	//	No device property to be read from database
	
	attr_TestValue_read = new Tango::DevDouble[1];
	/*----- PROTECTED REGION ID(TestServer::init_device) ENABLED START -----*/
	
	//	Initialize device
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::init_device
}


//--------------------------------------------------------
/**
 *	Method      : TestServer::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void TestServer::always_executed_hook()
{
	DEBUG_STREAM << "TestServer::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(TestServer::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : TestServer::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void TestServer::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "TestServer::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(TestServer::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : TestServer::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void TestServer::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "TestServer::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(TestServer::write_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::write_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute TestValue related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestServer::read_TestValue(Tango::Attribute &attr)
{
	DEBUG_STREAM << "TestServer::read_TestValue(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(TestServer::read_TestValue) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_TestValue_read);
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::read_TestValue
}
//--------------------------------------------------------
/**
 *	Write attribute TestValue related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void TestServer::write_TestValue(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "TestServer::write_TestValue(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(TestServer::write_TestValue) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::write_TestValue
}

//--------------------------------------------------------
/**
 *	Method      : TestServer::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void TestServer::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(TestServer::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : TestServer::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void TestServer::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(TestServer::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	TestServer::add_dynamic_commands
}

/*----- PROTECTED REGION ID(TestServer::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	TestServer::namespace_ending
} //	namespace
